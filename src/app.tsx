import * as ReactDOM from 'react-dom'
import * as React from 'react'
import Main from './components/Main'

export default class App {
  map: google.maps.Map

  constructor() {
    this.initialize()
    this.run()
  }

  initialize() {
    let mapOptions: google.maps.MapOptions = {
      zoom: 3,
      center: { lat: -28.024, lng: 140.887 },
    }
    this.map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions)
  }

  run() {
    ReactDOM.render(<Main map={this.map} />, document.getElementById('root'))
  }
}
