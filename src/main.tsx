import App from './app'
import React from 'react'

export function main() {
  const app = new App()
}

// support async tag or hmr
switch (document.readyState) {
  case 'interactive':
  case 'complete':
    main()
    break
  case 'loading':
  default:
    document.addEventListener('DOMContentLoaded', () => main())
}
