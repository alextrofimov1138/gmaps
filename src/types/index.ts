export enum SelectionMode {
  none = 'NONE',
  tempSelected = 'TEMP_SELECTED',
  selected = 'SELECTED',
}

export interface MarkerState {
  selectionMode?: SelectionMode
  starred?: boolean
  locationRevealed?: boolean
}

export interface MarkerOptions extends SlidingMarkerOptions {
  state?: MarkerState
}
