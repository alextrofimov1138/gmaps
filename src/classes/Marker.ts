import { MarkerOptions, MarkerState } from '../types'

export default class Marker extends SlidingMarker {
  private state?: MarkerState

  constructor(opts?: MarkerOptions) {
    super(opts)
    this.state = opts.state
  }

  setState(newState: MarkerState) {
    this.state = Object.assign({}, this.state, newState)
  }

  getState() {
    return this.state
  }
}
