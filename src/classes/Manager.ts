import Icon = google.maps.Icon
import IEasingType = jQuery.easing.IEasingType
import LatLng = google.maps.LatLng
import Marker from './Marker'
import { MarkerState, MarkerOptions, SelectionMode } from '../types'
import { mapStateToIcon } from '../helpers'

export default class Manager {
  private readonly map: google.maps.Map
  protected markers: Marker[]
  private selectionListener: (markerId: number, selectionMode: SelectionMode) => void
  private mapStateToIcon: (state: MarkerState) => Icon

  constructor(map: google.maps.Map) {
    this.map = map
    this.markers = []
    this.mapStateToIcon = mapStateToIcon
  }

  addMarker(options: MarkerOptions): number {
    let marker = new Marker(Object.assign({ map: this.map }, options))
    const { state } = options
    if (state) {
      marker.setIcon(this.mapStateToIcon(state))
    }
    this.markers.push(marker)
    return this.markers.length - 1
  }

  moveMarker(
    markerId: number,
    location: LatLng,
    { beginIcon, endIcon, easingType }: { beginIcon?: Icon; endIcon?: Icon; easingType?: IEasingType } = {},
  ): Promise<number> {
    const marker = this.markers[markerId]
    if (beginIcon) {
      marker.setIcon(beginIcon)
    }
    if (easingType) {
      marker.setEasing(easingType)
    }
    marker.setPosition(location)

    return new Promise((resolve) => {
      setTimeout(() => {
        if (endIcon) {
          marker.setIcon(endIcon)
        }
        resolve(markerId)
      }, marker.getDuration())
    })
  }

  setSelected(markerId: number, selectionMode: SelectionMode) {
    if (this.updateState(markerId, { selectionMode }) && this.selectionListener) {
      this.selectionListener(markerId, selectionMode)
    }
  }

  addSelectionListener(selectionListener: (markerId?: number, selectionMode?: SelectionMode) => void) {
    this.selectionListener = selectionListener
  }

  getState(markerId: number): MarkerState {
    return this.markers[markerId].getState()
  }

  updateState(markerId: number, newState: MarkerState): boolean {
    if (this.markers[markerId]) {
      this.markers[markerId].setState(newState)
      this.markers[markerId].setIcon(this.mapStateToIcon(newState))
      return true
    }
    return false
  }

  setMapStateToIcon(mapStateToIcon: (state: MarkerState) => Icon) {
    this.mapStateToIcon = mapStateToIcon
  }
}
