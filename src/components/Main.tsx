import * as React from 'react'
import Manager from '../classes/Manager'
import { SelectionMode } from '../types'

export default class Main extends React.Component<any, any> {
  state = {
    markers: [],
    selectedMarkerId: 0,
    messages: [],
  }
  private manager: Manager
  private inputLng: HTMLInputElement
  private inputLat: HTMLInputElement
  private modeSelector: HTMLSelectElement
  private inputNewLat: HTMLInputElement
  private inputNewLng: HTMLInputElement
  private inputBeginIcon: HTMLInputElement
  private inputEndIcon: HTMLInputElement
  private easingType: HTMLSelectElement
  private selectStarred: HTMLSelectElement
  private selectIsRevealed: HTMLSelectElement

  constructor(props) {
    super(props)

    this.manager = new Manager(props.map)
    this.manager.addSelectionListener((markerId, selectionMode) =>
      this.log(`SelectionListener: ${markerId} changes mode to ${selectionMode}`),
    )
  }

  componentDidMount(): void {
    this.onAdd({ position: new google.maps.LatLng(40, 40) })
  }

  onAdd(options) {
    this.setState({
      markers: [...this.state.markers, this.manager.addMarker(options)],
    })
  }

  onMove(location, { beginIcon, endIcon, easingType }) {
    this.manager
      .moveMarker(this.state.selectedMarkerId, location, { beginIcon, endIcon, easingType })
      .then((id) => this.log(`Movement finished by marker #${id}`))
  }

  onSetSelected(selectionMode) {
    this.manager.setSelected(this.state.selectedMarkerId, selectionMode)
  }

  onChangeState(newState) {
    this.manager.updateState(this.state.selectedMarkerId, newState)
  }

  log(message) {
    this.setState({
      messages: [message, ...this.state.messages],
    })
  }

  render() {
    const { markers, messages } = this.state

    return (
      <div className="wrapper">
        <div className="column">
          <input ref={(c) => (this.inputLat = c)} type="number" placeholder={'50'} />
          <input ref={(c) => (this.inputLng = c)} type="number" placeholder={'50'} />
          <button
            onClick={() =>
              this.onAdd({
                position: new google.maps.LatLng(Number(this.inputLat.value), Number(this.inputLng.value)),
              })
            }
          >
            Add marker
          </button>
        </div>

        <div className="column">
          <label htmlFor="markers">Choose a marker:</label>
          <select id="markers" onChange={(e) => this.setState({ selectedMarkerId: e.target.value })}>
            {markers.map((marker, index) => (
              <option value={index} key={index}>
                marker #{index + 1}
              </option>
            ))}
          </select>
        </div>

        <div className="column">
          <label htmlFor="markers">Selection mode:</label>
          <select id="markers" ref={(c) => (this.modeSelector = c)}>
            <option value={SelectionMode.none}>{SelectionMode.none}</option>
            <option value={SelectionMode.selected}>{SelectionMode.selected}</option>
            <option value={SelectionMode.tempSelected}>{SelectionMode.tempSelected}</option>
          </select>
          <button onClick={() => this.onSetSelected(this.modeSelector.value)}>Set mode</button>
        </div>
        <div className="column">
          <input ref={(c) => (this.inputNewLat = c)} type="number" placeholder={'70'} />
          <input ref={(c) => (this.inputNewLng = c)} type="number" placeholder={'70'} />
          <input ref={(c) => (this.inputBeginIcon = c)} type="text" placeholder={'Start Icon url'} />
          <input ref={(c) => (this.inputEndIcon = c)} type="text" placeholder={'End Icon url'} />
          <label htmlFor="easingType">Animation type:</label>
          <select id="easingType" ref={(c) => (this.easingType = c)}>
            {easings.map((name, index) => (
              <option value={name} key={index}>
                {name}
              </option>
            ))}
          </select>
          <button
            onClick={() =>
              this.onMove(
                new google.maps.LatLng(Number(this.inputNewLat.value), Number(this.inputNewLng.value)),
                {
                  beginIcon: this.inputBeginIcon.value,
                  endIcon: this.inputEndIcon.value,
                  easingType: this.easingType.value,
                },
              )
            }
          >
            Move marker
          </button>
        </div>

        <div className="column">
          <label htmlFor="starred">Is starred:</label>
          <select id="starred" ref={(c) => (this.selectStarred = c)}>
            <option value={''}>false</option>
            <option value={1}>true</option>
          </select>
          <label htmlFor="isRevealed">Is revealed:</label>
          <select id="isRevealed" ref={(c) => (this.selectIsRevealed = c)}>
            <option value={''}>false</option>
            <option value={1}>true</option>
          </select>
          <button
            onClick={() =>
              this.onChangeState({
                starred: !!this.selectStarred.value,
                locationRevealed: !!this.selectIsRevealed.value,
              })
            }
          >
            Set state
          </button>
        </div>
        <div className="column">
          <ul>
            {messages.map((message) => (
              <li>{message}</li>
            ))}
          </ul>
        </div>
      </div>
    )
  }
}

const easings = [
  'linear',
  'swing',
  'easeInQuad',
  'easeOutQuad',
  'easeInOutQuad',
  'easeInCubic',
  'easeOutCubic',
  'easeInOutCubic',
  'easeInQuart',
  'easeOutQuart',
  'easeInOutQuart',
  'easeInQuint',
  'easeOutQuint',
  'easeInOutQuint',
  'easeInSine',
  'easeOutSine',
  'easeInOutSine',
  'easeInExpo',
  'easeOutExpo',
  'easeInOutExpo',
  'easeInCirc',
  'easeOutCirc',
  'easeInOutCirc',
  'easeInElastic',
  'easeOutElastic',
  'easeInOutElastic',
  'easeInBack',
  'easeOutBack',
  'easeInOutBack',
  'easeInBounce',
  'easeOutBounce',
  'easeInOutBounce',
]
