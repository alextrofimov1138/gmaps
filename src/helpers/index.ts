import { MarkerState, SelectionMode } from '../types'

/**
 * example state to icon function
 * @param state
 */
export const mapStateToIcon = (state: MarkerState): any => {
  const { starred, locationRevealed, selectionMode } = state
  if (starred) {
    return '../assets/starred-marker.png'
  }
  if (selectionMode === SelectionMode.selected) {
    if (locationRevealed) {
      return '../assets/pin-marker.png'
    }
    return '../assets/checked-marker.png'
  }
  if (locationRevealed) {
    return '../assets/revealed-marker.png'
  }
}
