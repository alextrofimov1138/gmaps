const webpack = require('webpack');
const path = require('path');
const webpackMerge = require('webpack-merge');
const CopyWebpackPlugin = require('copy-webpack-plugin');

// Webpack Config
const webpackConfig = {
  entry: {
    'main': './src/main.tsx',
  },

  output: {
    publicPath: '',
    path: path.resolve(__dirname, './dist'),
  },

  plugins: [
     new CopyWebpackPlugin([
      { from: 'node_modules/marker-animate-unobtrusive/vendor/jquery.easing.1.3.js', to: 'vendor' },
      { from: 'node_modules/marker-animate-unobtrusive/vendor/markerAnimate.js', to: 'vendor' },
      { from: 'node_modules/marker-animate-unobtrusive/SlidingMarker.js' },
    ]),
  ],

  module: {
    rules: [
      // .ts files for TypeScript
      {
        test: /\.(ts|tsx)$/,
        loaders: [
          'awesome-typescript-loader'
        ]
      },
      { test: /\.css$/, loaders: ['to-string-loader', 'css-loader'] },
      { test: /\.html$/, loader: 'raw-loader' }
    ]
  }

};


// Our Webpack Defaults
const defaultConfig = {
  devtool: 'source-map',

  output: {
    filename: '[name].bundle.js',
    sourceMapFilename: '[name].map',
    chunkFilename: '[id].chunk.js'
  },

  resolve: {
    extensions: [ '.ts', '.tsx' ,'.js' ],
    modules: [ path.resolve(__dirname, 'node_modules') ]
  },

  devServer: {
    historyApiFallback: true,
    watchOptions: { aggregateTimeout: 300, poll: 1000 },
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
      "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization"
    }
  },

  node: {
    global: true,
    crypto: 'empty',
    __dirname: true,
    __filename: true,
    process: true,
    Buffer: false,
    clearImmediate: false,
    setImmediate: false
  }
};


module.exports = webpackMerge(defaultConfig, webpackConfig);
