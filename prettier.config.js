module.exports = {
  "semi": false,
  "singleQuote": true,
  "trailingComma": "all",
  "printWidth": 110,
  "arrowParens": "always"
}